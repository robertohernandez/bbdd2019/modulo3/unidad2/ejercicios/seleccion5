<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas de Seleccion 5';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de seleccion 5</h1>

        <p class="lead">Modulo 3 - unidad 2</p>
        
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.1 Active Record</h3>
                        <p>Nombre y edad de los ciclistas que No han ganado etapas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta1'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.2</h3>
                        <p>Listar nombre y edad de los ciclistas que no han ganado puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta2'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
              
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.3</h3>
                        <p>Listar nombre y edad de los ciclistas que han ganado puertos y etapas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta3'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.4</h3>
                        <p>dorsal y nombre de ciclistas que no hayan llevado algun maillot</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta4'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         <!------------------------------------------->   
         
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.5</h3>
                        <p>Dorsal y nombre de los ciclistas que han llevado algun maillot</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta5'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
   
            
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.6</h3>
                        <p>Listar Numero de etapa de las que no tengan puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta6'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.7</h3>
                        <p>Listar Distancia media de las etapas sin puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta7'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.8</h3>
                        <p>Listar numero de ciclistas que no hayan ganado alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta8'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.9</h3>
                        <p>Listar dorsales que hayan ganado alguna etapa que no tenga puerto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta9'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
           <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.10</h3>
                        <p>Listar ciclistas que hayan ganado unicamente etapas sin puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta10'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         
            

    </div>
</div>
