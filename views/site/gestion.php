<?php
use yii\helpers\Html; 

?>

<?= Html::a('Ciclistas', ['ciclista/index'], ['class' => 'btn btn-primary btn-large']) ?>
<?= Html::a('Equipo', ['equipo/index'], ['class' => 'btn btn-primary btn-large menu_crud']) ?>
<?= Html::a('Etapa', ['etapa/index'], ['class' => 'btn btn-primary btn-large menu_crud']) ?>
<?= Html::a('Lleva', ['lleva/index'], ['class' => 'btn btn-primary btn-large menu_crud']) ?>
<?= Html::a('Maillot', ['maillot/index'], ['class' => 'btn btn-primary btn-large menu_crud']) ?>
<?= Html::a('Puerto', ['puerto/index'], ['class' => 'btn btn-primary btn-large menu_crud']) ?>
