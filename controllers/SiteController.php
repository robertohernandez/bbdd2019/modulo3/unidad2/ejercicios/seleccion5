<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use app\models\Equipo;
use app\models\Maillot;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionCrud(){
        return $this->render("gestion");
    }   
     
    public function actionConsulta1(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand('SELECT count(*) FROM ciclista')
//                ->queryScalar();
//                
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre,edad FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL',
            //'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con Dao",
            "enunciado"=>"Listar nombre y edad de los ciclistas que no han ganado etapas",
            "sql"=>"SELECT nombre,edad FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    
    public function actionConsulta1a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->leftJoin('etapa','etapa.dorsal = ciclista.dorsal')
                 ->select("ciclista.nombre,ciclista.edad")->distinct()
                 ->where("etapa.dorsal IS NULL"),
           'pagination' => [
                'pageSize' =>6,
            ],   
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar nombre y edad de los ciclistas que no han ganado etapas",
            "sql"=>"SELECT nombre,edad FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL",
        ]);
      
    }
    
    public function actionConsulta2(){
        //mediante DAO         
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nombre,edad FROM ciclista LEFT JOIN puerto USING(dorsal) WHERE puerto.dorsal IS NULL",
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1.2 con Dao",
            "enunciado"=>"Listar nombre y edad de los ciclistas que no han ganado puertos",
            "sql"=>"SELECT nombre,edad FROM ciclista LEFT JOIN puerto USING(dorsal) WHERE puerto.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
               ->leftJoin('puerto','puerto.dorsal = ciclista.dorsal')
               ->select("ciclista.nombre,ciclista.edad")->distinct()
               ->where("puerto.dorsal IS NULL"),
             
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1.2 con Active Record",
            "enunciado"=>"Listar nombre y edad de los ciclistas que no han ganado puertos",
            "sql"=>"SELECT nombre,edad FROM ciclista LEFT JOIN puerto USING(dorsal) WHERE puerto.dorsal IS NULL",
        ]);
    }
    
    
    public function actionConsulta3(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo ='Artiach' or nomequipo ='Amore Vita'")
//                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT director FROM(
                            SELECT DISTiNCT nomequipo FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL)c1
                          JOIN equipo USING(nomequipo)",
//            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 1.3 con Dao",
            "enunciado"=>"Listar director que tenga ciclistas que no hayan ganado ninguna etapa",
            "sql"=>"SELECT DISTINCT director FROM(
                        SELECT DISTiNCT nomequipo FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL)c1
                      JOIN equipo USING(nomequipo)",
        ]);
    }
    
    
    
    public function actionConsulta3a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Equipo::find()
               ->select("director")->distinct()
               ->joinWith('ciclistas')  
               ->leftJoin('etapa','etapa.dorsal = ciclista.dorsal')
               ->where("etapa.dorsal IS NULL"),
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 1.3 con Active Record",
            "enunciado"=>"Listar director que tenga ciclistas que no hayan ganado ninguna etapa",
            "sql"=>"SELECT DISTINCT director FROM(
                        SELECT DISTiNCT nomequipo FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL)c1
                      JOIN equipo USING(nomequipo)",
        ]);
    
      
    }
    
    
     public function actionConsulta4(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo ='Artiach' or nomequipo ='Amore Vita'")
//                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal,nombre FROM ciclista LEFT JOIN lleva USING(dorsal) WHERE lleva.dorsal IS NULL",
//            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.4 con Dao",
            "enunciado"=>"Dorsal y nombre de los ciclistas que han llevado algun maillot",
            "sql"=>"SELECT dorsal,nombre FROM ciclista LEFT JOIN lleva USING(dorsal) WHERE lleva.dorsal IS NULL",
        ]);
    }
    
    
    
    public function actionConsulta4a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                ->select("ciclista.dorsal, ciclista.nombre")->distinct()
                ->joinWith('llevas')         
                ->where('lleva.dorsal is null'),
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.4 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que han llevado algun maillot",
            "sql"=>"SELECT dorsal,nombre FROM ciclista LEFT JOIN lleva USING(dorsal) WHERE lleva.dorsal IS NULL"
        ]);
    
      
    }
        public function actionConsulta5(){
        
         //mediante DAO
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista JOIN (
                                SELECT DISTINCT lleva.dorsal FROM lleva LEFT JOIN(
                                  SELECT DISTINCT dorsal FROM(
                                    SELECT código FROM maillot WHERE color = 'Amarillo')c1 JOIN lleva USING(código)
                                )c2 USING(dorsal) WHERE c2.dorsal IS NULL)c3 USING(dorsal)")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT ciclista.dorsal,nombre FROM ciclista JOIN (
                            SELECT DISTINCT lleva.dorsal FROM lleva LEFT JOIN(
                              SELECT DISTINCT dorsal FROM(
                                SELECT código FROM maillot WHERE color = 'Amarillo')c1 JOIN lleva USING(código)
                            )c2 USING(dorsal) WHERE c2.dorsal IS NULL)c3 USING(dorsal)",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.5 con Dao",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado el maillot amarillo nunca",
            "sql"=>"SELECT * FROM ciclista LEFT join(
                            SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN lleva USING(dorsal) WHERE código = 'MGE')c1
                            USING(dorsal) WHERE c1.dorsal IS NULL",
            
        ]);
    
    }
    
    public function actionConsulta5a(){
        //mediante Active Record
        // SUBCONSULTA ANTES DEL DATA PROVIDER
        
        $subconsulta = Maillot::find()
                 ->select("dorsal")->distinct()
                 ->innerJoin('lleva','lleva.código = maillot.código')
                 ->where("color = 'Amarillo'")->asArray()->all();
        
        $subconsulta=ArrayHelper::getColumn($subconsulta, "dorsal");
        $subconsulta=implode(",",$subconsulta);
                 
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("ciclista.dorsal,ciclista.nombre")->distinct()
                 ->innerJoin('lleva','ciclista.dorsal = lleva.dorsal')
                 ->where("lleva.dorsal not in($subconsulta)"),
             
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
     
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.5 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no han llevado nunca maillot amarillo",
            "sql"=>" SELECT DISTINCT dorsal,nombre FROM ciclista LEFT JOIN lleva USING(dorsal) WHERE lleva.código<>'MGE'",
        ]);
    
    }
    
    public function actionConsulta5b(){
        //mediante Active Record
        // SUBCONSULTA ANTES DEL DATA PROVIDER
        
        $subconsulta = Maillot::find()
                 ->select("dorsal")->distinct()
                 ->innerJoin('lleva','lleva.código = maillot.código')
                 ->where("color = 'Amarillo'");
        
         $query=Lleva::find()
                 ->select('lleva.dorsal')
                 ->distinct()
                 ->leftJoin(['p'=>$subconsulta],'p.dorsal=lleva.dorsal')
                 ->where("p.dorsal is null");
         
         $query1=Ciclista::find()
                 ->select("ciclista.dorsal,nombre")
                 ->innerJoin(['p1'=>$query],'p1.dorsal=ciclista.dorsal');
                
                 
         $dataProvider = new ActiveDataProvider([
             'query' => $query1,
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
     
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1.5 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no han llevado nunca maillot amarillo",
            "sql"=>" SELECT DISTINCT dorsal,nombre FROM ciclista LEFT JOIN lleva USING(dorsal) WHERE lleva.código<>'MGE'",
        ]);
    
    }
     
    public function actionConsulta6(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS null")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT etapa.numetapa etapas FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS null",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['etapas'],
            "titulo"=>"Consulta 1.6 con Dao",
            "enunciado"=>"Listar Numero de etapa de las que no tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS null",
        ]);
    
    }
    
    public function actionConsulta6a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Etapa::find()
                 ->select("etapa.numetapa")->distinct()
                 //->innerJoin('lleva','ciclista.dorsal = lleva.dorsal'),
                 ->leftJoin('puerto','puerto.numetapa = etapa.numetapa')
                 ->where("puerto.numetapa is null"),
                
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 1.6 con Active Record",
            "enunciado"=>"Listar Numero de etapa de las que no tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS null",
        ]);
    
    }
    public function actionConsulta7(){
        //mediante Dao
         $numero = 1;
//        $numero = yii::$app->db
//                ->createCommand("SELECT AVG(kms) FROM etapa JOIN(
//                        SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL)c1")
//                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(kms)DistanciaMedia FROM etapa JOIN(
                        SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL)c1",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['DistanciaMedia'],
            "titulo"=>"Consulta 1.7 con Dao",
            "enunciado"=>"Listar Distancia media de las etapas sin puertos",
            "sql"=>"SELECT AVG(kms) FROM etapa JOIN(SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL)c1",
            
        ]);
    
    }
    
    public function actionConsulta7a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Etapa::find()
                 ->select('avg(kms) MediaKms')
                 //->innerJoinWith('ciclista'),
                 ->leftJoin('puerto','puerto.numetapa = etapa.numetapa')
                 ->where('puerto.numetapa is null'),
             'pagination'=>[
                 'pageSize'=>0,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['MediaKms'],
            "titulo"=>"Consulta 1.7 con Active Record",
            "enunciado"=>"Listar Distancia media de las etapas sin puertos",
            "sql"=>"SELECT AVG(kms) FROM etapa JOIN(SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL)c1",
        ]);
    
    }
    
    
     public function actionConsulta8(){
        //mediante Dao
         
        $numero = 1;
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(*)n FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['n'],
            "titulo"=>"Consulta 1.8 con Dao",
            "enunciado"=>"Listar numero de ciclistas que no hayan ganado alguna etapa",
            "sql"=>"SELECT COUNT(*)n FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL",
            
        ]);
    
    }
    
    public function actionConsulta8a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
              ->select('count(distinct ciclista.dorsal) total')
             ->leftJoin('etapa','etapa.dorsal = ciclista.dorsal')
             ->where('etapa.dorsal is Null'),
            
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1.8 con Active Record",
            "enunciado"=>"Listar numero de ciclistas que no hayan ganado alguna etapa",
            "sql"=>"SELECT COUNT(*)n FROM ciclista LEFT JOIN etapa USING(dorsal) WHERE etapa.dorsal IS NULL",
        ]);
    
    }
    public function actionConsulta9(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT distinct(count(dorsal)) FROM etapa JOIN(SELECT DISTINCT numetapa FROM etapa LEFT JOIN puerto 
                                        USING(numetapa) WHERE puerto.numetapa IS NULL)c1 USING(numetapa)")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT dorsal FROM etapa JOIN(SELECT DISTINCT numetapa FROM etapa 
                                  LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL)
                                    c1 USING(numetapa)",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.9 con Dao",
            "enunciado"=>"Listar dorsales que hayan ganado alguna etapa que no tenga puerto",
            "sql"=>"SELECT DISTINCT dorsal FROM etapa JOIN(SELECT DISTINCT numetapa FROM etapa 
                                  LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL)
                                    c1 USING(numetapa)",
        ]);
    
    }
    
    public function actionConsulta9a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Etapa::find()
              ->select('etapa.dorsal')->distinct()
              ->leftJoin('puerto','etapa.numetapa = puerto.numetapa') 
              ->where("puerto.numetapa is null"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.9 con Active Record",
            "enunciado"=>"Listar dorsales que hayan ganado alguna etapa que no tenga puerto",
            "sql"=>"SELECT DISTINCT dorsal FROM etapa JOIN(SELECT DISTINCT numetapa FROM etapa 
                                  LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL)
                                    c1 USING(numetapa)",
        ]);
    
    }
    
    public function actionConsulta10(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT distinct(count(*)) FROM ciclista INNER JOIN 
                        (etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa) ON ciclista.dorsal = etapa.dorsal WHERE puerto.numetapa IS NULL; 
")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT ciclista.dorsal FROM ciclista INNER JOIN 
                        (etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa) 
                        ON ciclista.dorsal = etapa.dorsal WHERE puerto.numetapa IS NULL",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.10 con Dao",
            "enunciado"=>"Listar ciclistas que hayan ganado unicamente etapas sin puertos",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM ciclista INNER JOIN 
                        (etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa) 
                        ON ciclista.dorsal = etapa.dorsal WHERE puerto.numetapa IS NULL",
            
        ]);
    
    }
    
    public function actionConsulta10a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
               ->distinct()
               ->select('ciclista.dorsal')
               //->innerJoin('puerto','puerto.numetapa = etapa.numetapa') 
               ->innerJoinWith('etapas')
               ->leftJoin('puerto','etapa.numetapa = puerto.numetapa')
               ->where('puerto.numetapa is null'),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.10 con Active Record",
            "enunciado"=>"Listar total ciclistas que hayan ganado alguna etapa con puerto",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM ciclista INNER JOIN 
                        (etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa) 
                        ON ciclista.dorsal = etapa.dorsal WHERE puerto.numetapa IS NULL",
        ]);
    
    }
  
}
